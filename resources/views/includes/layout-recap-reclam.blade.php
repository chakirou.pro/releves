    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/boostrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Custom styles for this this page-->
    <link href="css/releve-reclam.css" rel="stylesheet">

<div class="container">
    <!--Diapo resume content-->
    <div class="navbar navbar-expand-lg navbar-white diapo-nav-style2 bg-white mb-5 bg-white rounded">
        <div class="row diapo-style2">
            <div class="col order-first  activ-box activ-etap1 diapo-cont">
                <img src="img/etape1.png"/>
            </div>
            <div class="col diapo-cont ">
                <img src="img/etape3.png"/>
            </div>
            <div class="col order-last diapo-cont">
                <img src="img/etape5.png"/>
            </div>
        </div>
    </div>
</div>
