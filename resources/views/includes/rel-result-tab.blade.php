<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
        <!--titres du tableau -->
        <!-- Contenu du tableau -->
        <thead>
            <th class="scope">#</th>
            <th class="scope">Nom</th>
            <th class="scope">Prenom</th>
            <th class="scope">Date dépôt</th>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Jim Boltron</td>
                <td>Boltron Boltron</td>
                <td>
                    21 Jan 2020
                </td>
            </tr>
        </tbody>
    </table>
</div>
